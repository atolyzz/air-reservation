class Flight {
  constructor(flightNumber, aircraft) {
    this.flightNumber = flightNumber;
    this.aircraft = aircraft;

    const rows = this.aircraft.getSeatingPlan()[0][1];
    const seats = this.aircraft.getSeatingPlan()[1];

    const temp = [];
    temp.length = rows;
    temp.fill("");

    this.seating = temp.map(el => {
      const row = seats.map(seat => {
        const s = {};
        s[seat] = undefined;
        return s;
      });

      return row;
    });
  }

  getAirline() {
    return this.flightNumber.substr(0, 2);
  }

  getFlightNumber() {
    return this.flightNumber.substr(2);
  }

  getAircraftModel() {
    return this.aircraft.getModel();
  }

  parseSeat(place) {
    const row = parseInt(place, 10);

    if (isNaN(row)) {
      console.log("incorrect row number");
      return false;
    }

    if (row < 0 || row > this.seating.length) {
      console.log("row doesnt exist");
      return false;
    }

    const seat = place.substr(-1);

    const ifInSeats = this.seating[0].filter(el => Object.keys(el)[0] === seat);

    if (ifInSeats.length === 0) {
      console.log("seat doesnt exist");
      return false;
    }

    return [row, seat];
  }

  allocateSeats(seat = "12C", passenger = "Paweł Konior") {
    const parsedSeat = this.parseSeat(seat);

    this.seating[parsedSeat[0]] = this.seating[parsedSeat[0]].map(el => {
      if (
        Object.keys(el)[0] === parsedSeat[1] &&
        Object.values(el)[0] === undefined
      ) {
        el[parsedSeat[1]] = passenger;
      }

      return el;
    });
  }
}

export default Flight;

[
  [
    {
      A: undefined
    },
    {
      B: undefined
    }
  ],
  [
    {
      A: undefined
    },
    {
      B: undefined
    }
  ]
];
