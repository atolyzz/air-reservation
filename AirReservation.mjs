import Flight from "./Flight.mjs";
import AirbusA319 from "./AirbusA319.mjs";
import Boeing777 from "./Boeing777.mjs";

const airbus = new AirbusA319();
const flight = new Flight("LO676", airbus);

const flight2 = new Flight("BA612", new Boeing777());

// console.log(flight.getAirline());
// console.log(flight.getFlightNumber());
// console.log(flight.getAircraftModel());

// console.log(flight2.getAirline());
// console.log(flight2.getFlightNumber());
// console.log(flight2.getAircraftModel());
