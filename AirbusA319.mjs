import Aircraft from "./Aircraft.mjs";

class AirbusA319 extends Aircraft {
  getModel() {
    return "AirbusA319";
  }

  getSeatingPlan() {
    return [[1, 24], ["A", "B", "C", "D", "E", "F"]];
  }

  getSeatNumber() {
    return super.getSeatNumber(this.getSeatingPlan());
  }
}

export default AirbusA319;
