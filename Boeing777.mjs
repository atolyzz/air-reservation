import Aircraft from "./Aircraft.mjs";

class Boeing777 extends Aircraft {
  getModel() {
    return "Boeing777";
  }

  getSeatingPlan() {
    return [[1, 56], ["A", "B", "C", "D", "E", "F", "G", "H", "I"]];
  }

  getSeatNumber() {
    return super.getSeatNumber(this.getSeatingPlan());
  }
}

export default Boeing777;
